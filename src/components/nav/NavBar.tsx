import { Component } from "react";
import styled, { ThemedStyledFunction } from "styled-components";
import { CV_EN, CV_SV } from "../../constants/CV_LINKS";
import { backgroundContext } from "../../background/Background";

const NavBarStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  h1 {
    padding: 2rem;
    font-size: 5rem;
  }
`;

export default class NavBar extends Component {
  constructor(props: Readonly<{}>) {
    super(props);
  }
  render() {
    const ButtonStyle = styled.div`
      margin: 1rem;
      padding: 0.5em;
      cursor: pointer;
      font-size: 1rem;
      font-weight: 400;
      line-height: 2rem;
      text-transform: uppercase;
      border: 1px solid;
      border-color: transparent;
      box-shadow: inset 0 0 20px rgba(255, 255, 255, 0);
      outline: 1px solid;
      outline-color: rgba(255, 255, 255, 0.5);
      outline-offset: 0px;
      text-shadow: none;
      transition: all 1s cubic-bezier(0.19, 1, 0.22, 1);
      ${typeof window !== "undefined" && "ontouchstart" in window
        ? ":active"
        : ":hover"} {
        border-color: white;
        box-shadow: inset 0 0 20px rgba(255, 255, 255, 0.5),
          0 0 20px rgba(255, 255, 255, 0.2);
        outline-color: rgba(255, 255, 255, 0);
        outline-offset: 2rem;
        text-shadow: 2px 2px 2px #427388;
      }
    `;
    const svgStyle = { padding: "1rem", width: "3rem", height: "3rem" };
    return (
      <NavBarStyle>
        <h1>
          Anton <br />
          Blomström
        </h1>
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <a href={CV_EN} target="_blank" rel="noreferrer">
            <ButtonStyle>
              <p>{"CV (EN)"}</p>
            </ButtonStyle>
          </a>
          <a
            href="mailto: email@antblo.com"
            target="_blank"
            rel="noreferrer"
          >
            <ButtonStyle>
              <p>{"Contact me"}</p>
            </ButtonStyle>
          </a>

          <a href={CV_SV} target="_blank" rel="noreferrer">
            <ButtonStyle>
              <p>{"CV (SV)"}</p>
            </ButtonStyle>
          </a>
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <a
            href={"https://gitlab.com/antonblomstrom"}
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/gitlab.svg"} alt={"gitlab"} style={svgStyle}></img>
          </a>
          <backgroundContext.Consumer>
            {(value) => {
              return <></>;
            }}
          </backgroundContext.Consumer>
          <a
            href={"https://www.linkedin.com/in/antonblomstrom97/"}
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/linkedin.svg"} alt={"LinkedIn"} style={svgStyle}></img>
          </a>
        </div>
      </NavBarStyle>
    );
  }
}
