import React, { Component, Fragment } from "react";
import styled from "styled-components";
import song_fft_file from "../../public/audio/song.fft";

import { AudioBackgroundObject } from "./AudioBackground";

import fragmentShader from "./background.frag";
import { Application, Container, IApplicationOptions } from "pixi.js";
import BackgroundAnimation from "./BackgroundAnimation";

type maybeBackgroundAnimation = null | BackgroundAnimation
export const backgroundContext = React.createContext<maybeBackgroundAnimation>(null);

const Shader = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
`;

export default class Background extends Component {
  ref: React.RefObject<HTMLDivElement>;
  backgroundAnimation: BackgroundAnimation;

  constructor(props: Readonly<{}>) {
    super(props);
    this.ref = React.createRef();
  }

  async componentDidMount() {
    this.backgroundAnimation = new BackgroundAnimation()
    await this.backgroundAnimation.load_shader(this.ref)
    this.ref.current?.appendChild(this.backgroundAnimation.app.view);
  }

  componentWillUnmount() {
    this.backgroundAnimation.dispose()
  }

  render() {
    return (
      <backgroundContext.Provider value={this.backgroundAnimation}>
        <Shader ref={this.ref} />
        {this.props.children}
      </backgroundContext.Provider>
    );
  }
}
