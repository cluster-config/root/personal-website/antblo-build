import { encode, decode } from "@msgpack/msgpack";
if (typeof window !== "undefined") var ctx = new window.AudioContext();

export class AudioBackgroundObject {
  maxVolume: number;
  audio: HTMLAudioElement;
  gainControl: GainNode;
  fft?: number[];
  constructor(audio_file: string, loop: boolean, fft_file?: Uint8Array) {
    this.audio = new Audio(audio_file);
    if (fft_file) {
      this.fft = decode(fft_file) as Array<number>;
    }
    let source = ctx.createMediaElementSource(this.audio);
    source.connect(ctx.destination);
    this.audio.loop = loop;
  }

  currentFFT(): number {
    if (this.fft && this.audio.duration) {
      let index = Math.floor(
        this.fft.length * (this.audio.currentTime / this.audio.duration)
      );
      return this.fft[index];
    } else return 0;
  }

  play() {
    ctx.resume();
    this.audio.play();
    this.audio.volume = 0;
  }

  pause() {
    this.audio.pause();
  }

  setVolume(volume: number) {
    this.audio.volume = volume;
  }
}
