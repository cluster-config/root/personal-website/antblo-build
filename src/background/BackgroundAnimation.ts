import { Application, Container } from "pixi.js";
import { AudioBackgroundObject } from "./AudioBackground";
import song_fft_file from "../../public/audio/song.fft";
import fragmentShader from "./background.frag";

export default class BackgroundAnimation {
  app: Application;
  uniforms: { time: number; song: number; rain_dist: number };
  container: Container;
  song: AudioBackgroundObject;
  rain: AudioBackgroundObject;
  startAt: number;
  startupSeconds: number;

  constructor() {
    this.startAt = 0;
    this.startupSeconds = 4;
  }

  load_song() {
    this.song = new AudioBackgroundObject("/audio/song.mp3", true, song_fft_file);
    this.rain = new AudioBackgroundObject("/audio/rain.webm", true);
  }

  resize() { this.app.renderer.resize(window.innerWidth, window.innerHeight) }

  async load_shader(ref: React.RefObject<HTMLDivElement>) {
    let PIXI = await import("pixi.js");
    let options = {
      autoStart: true,
      antialias: true,
      autoResize: true,
      resizeTo: document.body,
      backgroundColor: 0x000000,
      sharedTicker: true,
      sharedLoader: true
    }

    this.app = new PIXI.Application(options);
    window.addEventListener("resize", () => this.resize);

    this.uniforms = { time: 0.0, song: 0.0, rain_dist: 0.0 };
    this.container = new PIXI.Container();
    this.container.filterArea = this.app.renderer.screen;
    this.app.stage.addChild(this.container);

    this.app.ticker.add((delta) => {
      let startup = 0;
      startup = Math.min(
        1,
        (window.performance.now() - this.startAt) /
        (1000 * this.startupSeconds)
      );
      this.uniforms.time += delta * 0.001
      this.uniforms.rain_dist = startup;
    });

    const filter = new PIXI.Filter("", fragmentShader, this.uniforms);
    filter.autoFit = true;
    this.container.filters = [filter];
    ref.current?.appendChild(this.app.view);
    this.app.render();
    this.app.start()
  }

  dispose() {
    window.removeEventListener("resize", this.resize);
  }
}
