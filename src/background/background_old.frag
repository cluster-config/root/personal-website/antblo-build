precision lowp float;

uniform vec2 inputPixel;
uniform float time;
uniform float song;
uniform float rain_dist;

const float horizon = 0.2;

const float PHI = 1.61803398874989484820459;

float rand1D(in float x) {
  return fract(tan(abs(x * PHI - x) * 512.724));
}

float rand2D(in vec2 xy) {
  return fract(tan(distance(xy * PHI, xy) * 512.724) * xy.x);
}

float rand3D(in vec3 xy) {
  return fract(tan(distance(xy * PHI, xy) * 512.724) * xy.x);
}

float linear_map(float inp, float in_start, float in_end, float out_start, float out_end) {
  return ((inp - in_start) * (out_end - out_start)) / (in_end - in_start) + out_start;
}
float horizon_light(vec2 uv) {
  uv.y -= horizon;
  uv.x -= 0.5 * (inputPixel.x / inputPixel.y);
  return (song * 0.005) / length(uv);
}

float rain_layer(vec2 uv, float num_cols, float layer_id, float dist, float light) {
  float impact_y = horizon * dist;
  float rev_impact = (1.0 - impact_y);
  float rev_dist = (1.0 - dist * 0.6);
  float time_multiplier = 0.02 * rev_dist;
  vec2 st = uv;
  st.x *= num_cols;
  
  float cols_id = floor(st.x);
  st.x = fract(st.x);
  
  float random_y_shift = rand2D(vec2(cols_id, layer_id));
  
  float rain_time = time * time_multiplier + random_y_shift;
  float rain_time_id = floor(rain_time);
  
  float random_x_shift = rand3D(vec3(cols_id, layer_id, rain_time_id));
  
  float looped_rain_time = fract(rain_time);
  
  st.y -= impact_y;
  st.y /= rev_impact;
  st.x -= 0.25 + random_x_shift * 0.5;
  
  const float splash_duration = 0.2;
  
  float fall_pos = linear_map(looped_rain_time, 0.0, 1.0, 1.0, - splash_duration);
  float rim_pos = max(0.0, looped_rain_time - (1.0 - splash_duration)) / splash_duration;
  
  float droplet_scale = rev_dist * 0.2;
  vec2 droplet_pos = vec2(st.x * 4.0, (st.y - fall_pos - droplet_scale * splash_duration * 0.5) / droplet_scale);
  float droplet = (light * 2.0 + 0.2) * smoothstep(splash_duration * 0.5, 0.0, abs(length(droplet_pos))) * float(uv.y > impact_y);
  
  vec2 splash_asp = vec2(1.0, num_cols * 3.0 * (1.0 + dist * 0.9));
  float rim_width = log2(1.0 + 0.02 * rev_dist);
  float splash = (1.0 - rim_pos) * (light * 20.0 + 0.3) * smoothstep(rim_width, 0.0, abs(0.25 * rim_pos - rim_width - length(st * splash_asp))) * (1.0 - rim_pos);
  return + droplet + splash;
}

void main()
{
  vec2 uv = gl_FragCoord.xy / inputPixel.y;
  vec3 col = vec3(0.0);
  const int num_layers = 20;
  const float num_layers_float = float(num_layers);
  
  float light = horizon_light(uv);
  float mask = float(horizon > uv.y);
  col += light * (1.0 - mask);
  col += 5.0 * light * mask;
  col += mask * 0.003 ;
  float rain = 0.0;
  for(int layer_id = 0; layer_id < num_layers; layer_id ++ ) {
    float layer_id_float = float(layer_id);
    float dist = layer_id_float / num_layers_float;
    float num_columns = 3.0 + dist * 30.0;
    vec2 st = uv;
    st.x += rand1D(layer_id_float);
    float drop = rain_layer(st , num_columns, layer_id_float, dist, light);
    float fadein = layer_id_float - clamp(num_layers_float - rain_dist * num_layers_float, layer_id_float - 1.0, layer_id_float);
    rain = max(rain, drop * fadein);
  }
  col += rain;
  //col += rand(uv);
  gl_FragColor = vec4(col, 1.0);
}
