import React from "react";
import { backgroundContext } from "../src/background/Background";

export default function Home() {
  if (typeof window !== "undefined") {
    return <backgroundContext.Consumer>
      {(value) => {
        return <></>;
      }}
    </backgroundContext.Consumer>
  } else {
    return <div></div>
  }
}
