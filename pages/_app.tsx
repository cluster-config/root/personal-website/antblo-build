import { createGlobalStyle } from "styled-components";
import React from "react";
import { Helmet } from "react-helmet";
import Background from "../src/background/Background";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    text-decoration: none;
    background-color: transparent;
    border: 0 transparent;
    outline:none;
  }
  body {
    font-size: clamp(12px,2vw,18px);
    background-color: black;
    width:100vw;
    height:100vh;
    overflow:hidden;
  }
  h1,h2,text,p,a{
    font-family: 'Times New Roman', Times, serif;
    color: #999;
  }
`;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <Helmet>
        <html lang={"en-us"} />
        <title>AntBlo</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta
          name="Description"
          content="Anton Blomström is a software developer who dabble in everything from deep learning to website development. Currently looking for a developer job."
        ></meta>
      </Helmet>
      {<Component {...pageProps} />}
      <Background></Background>
    </>
  );
}

export default MyApp;
