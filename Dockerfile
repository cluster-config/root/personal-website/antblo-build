FROM alpine:3.11.3 AS base
RUN apk update && apk upgrade && apk add npm

FROM base AS debug
RUN npm i
RUN apk add git

FROM base AS build
ADD . /build
WORKDIR /build
RUN npm i && npm run build

FROM base
COPY --from=BUILD /build/out /antblo
RUN npm i serve -g
EXPOSE 5000
CMD serve /antblo

