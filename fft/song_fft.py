import librosa
import matplotlib.pyplot as plt  # plotting
import msgpack
import numpy as np  # fast vectors and matrices
from librosa import stft


def movingaverage(interval, window_size):
    window = np.ones(int(window_size)) / float(window_size)
    return np.convolve(interval, window, 'same')


audio, fs = librosa.load('../src/background/audio/song.mp3', sr=None)
X = audio

# fs = 44100  # assumed sample frequency in Hz
window_size = 512  # 2048-sample fourier windows
stride = 512  # 512 samples between windows
wps = fs / float(stride)  # ~86 windows/second
Xs = np.empty([int(2 * wps), window_size])

X_libs = stft(X, n_fft=window_size, hop_length=stride)
X_libs = np.log(np.abs(X_libs))

arr = X_libs[70:100].max(0)
cuttoff = -3.6
arr[arr < cuttoff] = cuttoff
arr_min = np.min(arr)
arr = arr - np.min(arr)
arr = movingaverage(arr, 256)
arr = (arr - np.min(arr)) / (np.max(arr) - np.min(arr))

fig = plt.figure(figsize=(16, 7))
# plt.imshow(X_libs, aspect='auto')
plt.plot(arr)
# plt.gca().invert_yaxis()
fig.axes[0].set_xlabel('windows (~' + str(wps) + 'Hz)')
fig.axes[0].set_ylabel('frequency')
plt.show()

packed_arr = msgpack.packb(arr.tolist())

with open('../src/background/audio/song.fft', 'wb') as f:
    f.write(packed_arr)
print(packed_arr)
